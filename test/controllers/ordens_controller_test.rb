require 'test_helper'

class OrdensControllerTest < ActionDispatch::IntegrationTest
  setup do
    @orden = ordens(:one)
  end

  test "should get index" do
    get ordens_url, as: :json
    assert_response :success
  end

  test "should create orden" do
    assert_difference('Orden.count') do
      post ordens_url, params: { orden: { precio_unidad: @orden.precio_unidad, status: @orden.status, total: @orden.total } }, as: :json
    end

    assert_response 201
  end

  test "should show orden" do
    get orden_url(@orden), as: :json
    assert_response :success
  end

  test "should update orden" do
    patch orden_url(@orden), params: { orden: { precio_unidad: @orden.precio_unidad, status: @orden.status, total: @orden.total } }, as: :json
    assert_response 200
  end

  test "should destroy orden" do
    assert_difference('Orden.count', -1) do
      delete orden_url(@orden), as: :json
    end

    assert_response 204
  end
end
