require 'test_helper'

class DetalleOrdensControllerTest < ActionDispatch::IntegrationTest
  setup do
    @detalle_orden = detalle_ordens(:one)
  end

  test "should get index" do
    get detalle_ordens_url, as: :json
    assert_response :success
  end

  test "should create detalle_orden" do
    assert_difference('DetalleOrden.count') do
      post detalle_ordens_url, params: { detalle_orden: { relleno_id: @detalle_orden.relleno_id, tipo: @detalle_orden.tipo, total: @detalle_orden.total } }, as: :json
    end

    assert_response 201
  end

  test "should show detalle_orden" do
    get detalle_orden_url(@detalle_orden), as: :json
    assert_response :success
  end

  test "should update detalle_orden" do
    patch detalle_orden_url(@detalle_orden), params: { detalle_orden: { relleno_id: @detalle_orden.relleno_id, tipo: @detalle_orden.tipo, total: @detalle_orden.total } }, as: :json
    assert_response 200
  end

  test "should destroy detalle_orden" do
    assert_difference('DetalleOrden.count', -1) do
      delete detalle_orden_url(@detalle_orden), as: :json
    end

    assert_response 204
  end
end
