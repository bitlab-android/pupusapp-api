require 'test_helper'

class RellenosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @relleno = rellenos(:one)
  end

  test "should get index" do
    get rellenos_url, as: :json
    assert_response :success
  end

  test "should create relleno" do
    assert_difference('Relleno.count') do
      post rellenos_url, params: { relleno: { nombre: @relleno.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show relleno" do
    get relleno_url(@relleno), as: :json
    assert_response :success
  end

  test "should update relleno" do
    patch relleno_url(@relleno), params: { relleno: { nombre: @relleno.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy relleno" do
    assert_difference('Relleno.count', -1) do
      delete relleno_url(@relleno), as: :json
    end

    assert_response 204
  end
end
