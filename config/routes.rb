Rails.application.routes.draw do
  resources :detalle_ordens
  resources :ordens
  resources :rellenos
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
