class AddOrdenToDetalleOdern < ActiveRecord::Migration[6.0]
  def change
    add_reference :detalle_ordens, :orden, index: true
  end
end
