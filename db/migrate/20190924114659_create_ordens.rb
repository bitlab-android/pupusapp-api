class CreateOrdens < ActiveRecord::Migration[6.0]
  def change
    create_table :ordens do |t|
      t.integer :status, default: 1
      t.decimal :precio_unidad, precision: 10, scale: 2
      t.decimal :total, precision: 10, scale: 2

      t.timestamps
    end
  end
end
