class CreateRellenos < ActiveRecord::Migration[6.0]
  def change
    create_table :rellenos do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
