class CreateDetalleOrdens < ActiveRecord::Migration[6.0]
  def change
    create_table :detalle_ordens do |t|
      t.integer :tipo, default: 1
      t.references :relleno, null: false, foreign_key: true
      t.integer :total

      t.timestamps
    end
  end
end
