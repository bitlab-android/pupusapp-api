class RellenosController < ApplicationController
  before_action :set_relleno, only: [:show, :update, :destroy]

  # GET /rellenos
  def index
    @rellenos = Relleno.all

    render json: @rellenos, root: 'rellenos'
  end

  # GET /rellenos/1
  def show
    render json: @relleno
  end

  # POST /rellenos
  def create
    @relleno = Relleno.new(relleno_params)

    if @relleno.save
      render json: @relleno, status: :created, location: @relleno
    else
      render json: @relleno.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rellenos/1
  def update
    if @relleno.update(relleno_params)
      render json: @relleno
    else
      render json: @relleno.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rellenos/1
  def destroy
    @relleno.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_relleno
      @relleno = Relleno.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def relleno_params
      params.require(:relleno).permit(:nombre)
    end
end
