class OrdensController < ApplicationController
  before_action :set_orden, only: [:show, :update, :destroy]

  # GET /ordens
  def index
    @ordens = Orden.all

    render json: @ordens
  end

  # GET /ordens/1
  def show
    render json: @orden
  end

  # POST /ordens
  def create
    @orden = Orden.new(orden_params)

    # binding.pry

    if @orden.save
      if params['orden']['maiz'].count>0
        params['orden']['maiz'].each do |det_orden|
          DetalleOrden.create(total: det_orden['total'], tipo: 'maiz', relleno_id: det_orden['relleno']['id'], orden_id: @orden.id)
          # binding.pry
        end
      end
      if params['orden']['arroz'].count>0
        params['orden']['arroz'].each do |det_orden|
          DetalleOrden.create(total: det_orden['total'], tipo: 'arroz', relleno_id: det_orden['relleno']['id'], orden_id: @orden.id)
          # binding.pry
        end
      end
      
      render json: @orden, status: :created, location: @orden
    else
      render json: @orden.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ordens/1
  def update
    if @orden.update(orden_params)
      render json: @orden
    else
      render json: @orden.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ordens/1
  def destroy
    @orden.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orden
      @orden = Orden.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def orden_params
      params.require(:orden).permit(:status, :precio_unidad, :total, :maiz => [], :arroz => [])
    end
end
