class DetalleOrdensController < ApplicationController
  before_action :set_detalle_orden, only: [:show, :update, :destroy]

  # GET /detalle_ordens
  def index
    @detalle_ordens = DetalleOrden.all

    render json: @detalle_ordens
  end

  # GET /detalle_ordens/1
  def show
    render json: @detalle_orden
  end

  # POST /detalle_ordens
  def create
    @detalle_orden = DetalleOrden.new(detalle_orden_params)

    if @detalle_orden.save
      render json: @detalle_orden, status: :created, location: @detalle_orden
    else
      render json: @detalle_orden.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /detalle_ordens/1
  def update
    if @detalle_orden.update(detalle_orden_params)
      render json: @detalle_orden
    else
      render json: @detalle_orden.errors, status: :unprocessable_entity
    end
  end

  # DELETE /detalle_ordens/1
  def destroy
    @detalle_orden.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_detalle_orden
      @detalle_orden = DetalleOrden.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def detalle_orden_params
      params.require(:detalle_orden).permit(:tipo, :relleno_id, :total)
    end
end
