class Orden < ApplicationRecord
    has_many :detalle_ordens
    enum status: { recibida: 1, preparando: 2, en_camino: 3, lista_para_recoger: 4 }

end

# == Schema Information
#
# Table name: ordens
#
#  id            :bigint           not null, primary key
#  precio_unidad :decimal(10, 2)
#  status        :integer          default("recibida")
#  total         :decimal(10, 2)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
