class DetalleOrden < ApplicationRecord
  belongs_to :relleno
  enum tipo: { maiz: 1, arroz: 2}

end

# == Schema Information
#
# Table name: detalle_ordens
#
#  id         :bigint           not null, primary key
#  tipo       :integer          default(1)
#  total      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  orden_id   :bigint
#  relleno_id :bigint           not null
#
# Indexes
#
#  index_detalle_ordens_on_orden_id    (orden_id)
#  index_detalle_ordens_on_relleno_id  (relleno_id)
#
# Foreign Keys
#
#  fk_rails_...  (relleno_id => rellenos.id)
#
