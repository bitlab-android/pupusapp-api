class RellenoSerializer < ActiveModel::Serializer
  attributes :id, :nombre

end

# == Schema Information
#
# Table name: rellenos
#
#  id         :bigint           not null, primary key
#  nombre     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
