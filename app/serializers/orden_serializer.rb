class OrdenSerializer < ActiveModel::Serializer
  attributes :id, :status, :precio_unidad, :total, :maiz, :arroz
  
  def maiz
    object.detalle_ordens.where(tipo: 'maiz')
  end
  
  def arroz
    object.detalle_ordens.where(tipo: 'arroz')
  end
  
end

# == Schema Information
#
# Table name: ordens
#
#  id            :bigint           not null, primary key
#  precio_unidad :decimal(10, 2)
#  status        :integer          default("recibida")
#  total         :decimal(10, 2)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
